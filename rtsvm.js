// nodejs

Machine = function(memory, commands){
    var self = this;
    var cmds = commands.map((a)=>a.func);
    var cmds_argc = commands.map((a)=>a.argc);
    self.eip = 0;
    self.eflags = 0;
    self.memory = memory;
    self.start = function(){
        while(true) exec_single();
    }
    
    var membound = memory.length-1;
    function next_cell() {
        var result = self.memory[self.eip];
        if (self.eip==membound) self.eip=0;
        else self.eip++;
        return result;
    }
    
    function exec_single(){
        var cmd = next_cell();
        var argc = cmds_argc[cmd];
        var args = [self];
        for(var i=0;i<argc;i++) args.push(next_cell());
        cmds[cmd].apply(null, args);
    }
}

Compiler = function(commands){
    var self = this;
    var cmds_as_ids = commands.map((a)=>a.name);
    var cmds_argc = commands.map((a)=>a.argc);

    self.compile = function(text){
        if (text.match(/[^\n\s\w;,]/g)) 
            throw 'You dont know how 2 use this';
        var bytecode = [];
        text.replace(/[\n\s]/g,'').split(';').filter((a)=>a).forEach(function(token){
            var ops = token.split(',');
            var cmd = ops.shift();
            var id = cmds_as_ids.indexOf(cmd);
            if (id==-1) throw 'Cant recognize cmd: '+cmd;
            bytecode.push(id);
            if (cmds_argc[id]!=ops.length)
                throw 'Invalid number of arguments'
            for (var i=0;i<cmds_argc[id];i++){
                var arg = ops.shift()-0;
                if (isNaN(arg))
                    throw 'Instruction arguments must be numbers';
                bytecode.push(arg);
            }
        });
        return bytecode;
    }
}

var commands = [
    {name:'nop', argc: 0, func: function(machine){}},
    {name:'int', argc: 0, func: function(machine){throw 'Interrupt signal received'}},
    {name:'jmp', argc: 1, func: function(machine,addr){machine.eip=addr}},
    {name:'dmp', argc: 1, func: function(machine,addr){console.log(machine.memory[addr])}}, 
    {name:'sum', argc: 3, func: function(machine,a,b,res){
        machine.memory[res] = machine.memory[a]+machine.memory[b];
    }},
    {name:'sub', argc: 3, func: function(machine,a,b,res){
        machine.memory[res] = machine.memory[a]-machine.memory[b];
    }},
    {name:'mul', argc: 3, func: function(machine,a,b,res){
        machine.memory[res] = machine.memory[a]*machine.memory[b];
    }},
    {name:'div', argc: 3, func: function(machine,a,b,res){
        machine.memory[res] = Math.round(machine.memory[a]/machine.memory[b]);
    }},
    {name:'store', argc: 2, func: function(machine,addr,value){
        machine.memory[addr] = value;
    }},
    {name:'inc', argc: 1, func: function(machine,addr){
        machine.memory[addr]++;
    }},
    {name:'dec', argc: 1, func: function(machine,addr){
        machine.memory[addr]--;
    }},
    {name:'cmp', argc: 2, func: function(machine,a,b){
        machine.eflags = machine.memory[a]-machine.memory[b];
    }},
    {name:'je', argc: 1, func: function(machine,addr){
        if (machine.eflags==0) machine.eip = addr;
    }},
    {name:'jne', argc: 1, func: function(machine,addr){
        if (machine.eflags!=0) machine.eip = addr;
    }},
    {name:'jg', argc: 1, func: function(machine,addr){
        if (machine.eflags>0) machine.eip = addr;
    }},
    {name:'jng', argc: 1, func: function(machine,addr){
        if (machine.eflags<=0) machine.eip = addr;
    }},
    {name:'step', argc: 1, func: function(machine,amount){
        machine.eip+=amount;
    }},
    {name:'stepback', argc: 1, func: function(machine,amount){
        machine.eip-=amount;
    }},
    {name:'copy', argc: 2, func: function(machine,from,to){
        machine.memory[to] = machine.memory[from];
    }}
];

function create_memory(size){
    var result = [];
    for(var i=0;i<=size;i++) result.push(0);
    return result;
}

function run_machine(machine, code){ 
    try {
        var bytecode = compiler.compile(code).concat(interrupt);
        if (bytecode.length>machine.memory.length)
            throw 'I dont have a lot of memory...';
        for (var i=0;i<bytecode.length;i++)
            machine.memory[i]=bytecode[i];
        machine.eip=0;
        machine.start();
    }
    catch(e){
        console.log('EIP:'+machine.eip+':'+e);
    }
}

var compiler = new Compiler(commands);
var machine = new Machine(create_memory(30000), commands); //~240kb



var input_fname = process.argv[2];
var interrupt = compiler.compile('int');
if (!input_fname){
    var shell = require('readline').createInterface(process.stdin, process.stdout);
    shell.setPrompt('>>> ');
    shell.on('line', (line)=>{
        run_machine(machine, line);
        shell.prompt()
    });
    shell.prompt();
}
else
{
    var code = require('fs').readFileSync(input_fname, 'utf8');
    run_machine(machine, code.replace(/\r/g,'\n'));
}
